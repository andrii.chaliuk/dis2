package org.example;

import org.neo4j.driver.Record;
import org.neo4j.driver.*;

import java.io.*;
import java.util.Random;
import java.util.Scanner;

public class Neo4j {

    private static final String resPath = "src/main/results/neo4j/";
    private static final String dataFile = "src/main/docker/neo4j-init.txt";
    private static final Random random = new Random();

    public static void generateTestDate() throws IOException {
        int usersNum = 1000;
        int industriesNum = 1000;
        int jobNum = 1000;
        int companyNum = 1000;
        int jobRecordNum = 1000;
        int jobRecordResumeNum = 10000;
        int contactTypeNum = 1000;
        int contactInfoNum = 1000;
        int hobbyNum = 1000;
        int resumeNum = 1000;
        int hobbyResumeNum = 10000;

        String[] cities = {"Lviv", "Kyiv", "Odesa", "Kharkiv", "A", "B", "C", "D", "E", "F", "G"};

        FileWriter fileWriter = new FileWriter(dataFile);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        for (int i = 0; i < usersNum; ++i) {
            printWriter.printf("CREATE (:User {id: %d, login: '%s', password: '%s'})\n", i, "Login" + i, "Password" + i);
        }

        for (int i = 0; i < cities.length; ++i) {
            printWriter.printf("CREATE (:City {id: %d, city_name: '%s'})\n", i,  cities[i]);
        }

        for (int i = 0; i < industriesNum; ++i) {
            printWriter.printf("CREATE (:Industry {id: %d, industry_name: '%s'})\n", i, "Industry" + i);
        }

        for (int i = 0; i < jobNum; ++i) {
            printWriter.printf("CREATE (:Job {id: %d, job_title: '%s'})\n", i, "Job" + i);
        }

        for (int i = 0; i < companyNum; ++i) {
            printWriter.printf("CREATE (:Company {id: %d, company_name: '%s'})\n", i, "Company" + i);
        }

        for (int i = 0; i < jobRecordNum; ++i) {
            int company = random.nextInt(companyNum);
            int resumeConnectNum = random.nextInt(100);
            StringBuilder resume = new StringBuilder("[");
            for (int j = 0; j < resumeConnectNum; ++j) {
                int resumeId = random.nextInt(resumeNum);
                resume.append(resumeId);
                if (j < resumeConnectNum - 1) {
                    resume.append(",");
                }
            }
            resume.append("]");
            printWriter.printf("CREATE (:JobRecord {id: %d, company_id: %d, start_date: '%s', end_date: '%s', resume: %s})\n",
                    i, company, "2020-09-01", "2024-06-30", resume);
        }

        for (int i = 0; i < contactTypeNum; ++i) {
            printWriter.printf("CREATE (:ContactType {id: %d, type: '%s'})\n", i, "ContactType" + i);
        }

        for (int i = 0; i < contactInfoNum; ++i) {
            int type = random.nextInt(contactTypeNum);
            printWriter.printf("CREATE (:ContactInfo {id: %d, type_id: %d, url: '%s'})\n", i, type, "URL" + i);
        }

        for (int i = 0; i < hobbyNum; ++i) {
            int resumeConnectNum = random.nextInt(100);
            StringBuilder resume = new StringBuilder("[");
            for (int j = 0; j < resumeConnectNum; ++j) {
                int resumeId = random.nextInt(resumeNum);
                resume.append(resumeId);
                if (j < resumeConnectNum -1) {
                    resume.append(",");
                }
            }
            resume.append("]");
            printWriter.printf("CREATE (:Hobby {id: %d, hobby_type: '%s', resume: %s})\n", i, "Hobby" + i, resume);
        }

        for (int i = 0; i < resumeNum; ++i) {
            int user = random.nextInt(usersNum);
            int job = random.nextInt(jobNum);
            int industry = random.nextInt(industriesNum);
            int city = random.nextInt(cities.length);
            int company = random.nextInt(companyNum);
            int contactInfo = random.nextInt(contactInfoNum);
            printWriter.printf("CREATE (:Resume {id: %d, first_name: '%s', last_name: '%s'," +
                    " user_id: %d, position_id: %d, industry_id: %d, city_id: %d, company_id: %d," +
                    " contact_info_id: %d, date_of_creation: '%s'})\n",
                    i, "FirstName" + i, "LastName" + i,
                    user, job, industry, city, company, contactInfo, "2023-10-01");
        }

        printWriter.println("MATCH (e:JobRecord), (r:Resume) WHERE r.id In e.resume CREATE (e)-[:JOBRECORDRESUMECONNECT]->(r)");
        printWriter.println("MATCH (e:JobRecord), (r:Resume) WHERE r.id In e.resume CREATE (r)-[:RESUMEJOBRECORDCONNECT]->(e)");

        printWriter.println("MATCH (h:Hobby), (r:Resume) WHERE r.id In h.resume CREATE (h)-[:HOBBIES]->(r)");
        printWriter.println("MATCH (h:Hobby), (r:Resume) WHERE r.id In h.resume CREATE (r)-[:RESUME]->(h)");

        printWriter.println("MATCH (jr:JobRecord), (c:Company) WHERE jr.company_id = c.id CREATE (jr)-[:COMPANY]->(c)");
        printWriter.println("MATCH (jr:JobRecord), (c:Company) WHERE jr.company_id = c.id CREATE (c)-[:JOBRECORD]->(jr)");

        printWriter.println("MATCH (ci:ContactInfo), (ct:ContactType) WHERE ci.type_id = ct.id CREATE (ci)-[:TYPE]->(ct)");
        printWriter.println("MATCH (ci:ContactInfo), (ct:ContactType) WHERE ci.type_id = ct.id CREATE (ct)-[:CONTACTINFO]->(ci)");

        printWriter.println("MATCH (r:Resume), (u:User) WHERE r.user_id = u.id CREATE (r)-[:USER]->(u)");
        printWriter.println("MATCH (r:Resume), (u:User) WHERE r.user_id = u.id CREATE (u)-[:RESUME]->(r)");

        printWriter.println("MATCH (r:Resume), (j:Job) WHERE r.position_id = j.id CREATE (r)-[:JOB]->(j)");
        printWriter.println("MATCH (r:Resume), (j:Job) WHERE r.position_id = j.id CREATE (j)-[:RESUME]->(r)");

        printWriter.println("MATCH (r:Resume), (i:Industry) WHERE r.industry_id = i.id CREATE (r)-[:INDUSTRY]->(i)");
        printWriter.println("MATCH (r:Resume), (i:Industry) WHERE r.industry_id = i.id CREATE (i)-[:RESUME]->(r)");

        printWriter.println("MATCH (r:Resume), (c:City) WHERE r.city_id = c.id CREATE (r)-[:CITY]->(c)");
        printWriter.println("MATCH (r:Resume), (c:City) WHERE r.city_id = c.id CREATE (c)-[:RESUME]->(r)");

        printWriter.println("MATCH (r:Resume), (c:Company) WHERE r.company_id = c.id CREATE (r)-[:COMPANY]->(c)");
        printWriter.println("MATCH (r:Resume), (c:Company) WHERE r.company_id = c.id CREATE (c)-[:RESUME]->(r)");

        printWriter.println("MATCH (r:Resume), (ci:ContactInfo) WHERE r.contact_info_id = ci.id CREATE (r)-[:CONTACTINFO]->(ci)");
        printWriter.println("MATCH (r:Resume), (ci:ContactInfo) WHERE r.contact_info_id = ci.id CREATE (ci)-[:RESUME]->(r)");

        System.out.println("generated");

        printWriter.close();
    }

    public static void addGeneratedData(Session session) throws IOException {
        generateTestDate();

        File myObj = new File(dataFile);
        Scanner myReader = new Scanner(myObj);
        while (myReader.hasNextLine()) {
            String data = myReader.nextLine();
            Result result = session.run(data);
        }

        System.out.println("added");

        myReader.close();
    }

    public static void task1(Session session, String fileName) throws IOException {
        String query = "MATCH n=(Resume)-[]->() RETURN n AS resume";
        long startTime = System.currentTimeMillis();
        Result result = session.run(query);
        long endTime = System.currentTimeMillis();
        System.out.println("Query:" + (endTime-startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (result.hasNext()) {
            Record record = result.next();
            String res = "{\n\tPath:" + record.get("resume").asPath().start().values() + "\n\t"
                    + record.get("resume").asPath().toString() + "\n\t"
                    + record.get("resume").asPath().end().values() + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task2(Session session, String fileName) throws IOException {
        String query = "MATCH (n:Hobby) RETURN n AS hobby";

        long startTime = System.currentTimeMillis();
        Result result = session.run(query);
        long endTime = System.currentTimeMillis();
        System.out.println("Query:" + (endTime-startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (result.hasNext()) {
            Record record = result.next();
            String res = "{\n\tHobby:" + record.get("hobby").asEntity().values() + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task3(Session session, String fileName) throws IOException {
        String query = "MATCH (n:City) RETURN n AS city";

        long startTime = System.currentTimeMillis();
        Result result = session.run(query);
        long endTime = System.currentTimeMillis();
        System.out.println("Query:" + (endTime-startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (result.hasNext()) {
            Record record = result.next();
            String res = "{\n\tcity:" + record.get("city").asEntity().values() + "\n}";

            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task4(Session session, String city, String fileName) throws IOException {
        String query = "MATCH (n:Hobby)-[]->(r:Resume)-[]->(c:City) WHERE c.city_name = \"" + city + "\" RETURN n AS hobby";

        long startTime = System.currentTimeMillis();
        Result result = session.run(query);
        long endTime = System.currentTimeMillis();
        System.out.println("Query:" + (endTime-startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (result.hasNext()) {
            Record record = result.next();
            String res = "{\n\thobby:" + record.get("hobby").asEntity().values() + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task5(Session session, String fileName) throws IOException {
        String query = "MATCH (e:JobRecord)-[]->(r:Resume)-[]->(u:User), " +
                "(e)-[]->(r2:Resume)-[]->(u2:User) WHERE " +
                "u.id <> u2.id AND r2 <> r RETURN DISTINCT u AS user, e AS jobRecord";
        long startTime = System.currentTimeMillis();
        Result result = session.run(query);
        long endTime = System.currentTimeMillis();
        System.out.println("Query:" + (endTime-startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (result.hasNext()) {
            Record record = result.next();
            String res = "{\n\tuser: " + record.get("user").asEntity().values() + "\n\t" +
                    "jobRecord:" + record.get("jobRecord").asEntity().values() + "\n}";

            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task5_2(Session session, String fileName) throws IOException {
        String query = "MATCH (e:JobRecord)-[]->(r:Resume)-[]->(u:User)" +
                " RETURN DISTINCT u AS user, e AS jobRecord ORDER BY e.company_id ASC";
        long startTime = System.currentTimeMillis();
        Result result = session.run(query);
        long endTime = System.currentTimeMillis();
        System.out.println("Query:" + (endTime-startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (result.hasNext()) {
            Record record = result.next();
            String res = "{\n\tuser: " + record.get("user").asEntity().values() + "\n\t" +
                    "jobRecord:" + record.get("jobRecord").asEntity().values() + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void main(String[] args) {
        String uri = "neo4j+s://b8d29e5a.databases.neo4j.io";
        String user = "neo4j";
        String password = "HNGCa00_ZdKF9gYzYZMRoiF7eaOd-rTTcKixa7nuFOA";

        try (Driver driver = GraphDatabase.driver(uri, AuthTokens.basic(user, password));
             Session session = driver.session()) {

            //addGeneratedData(session);

            System.out.println("_______________TASK1______________");
            long startTime = System.currentTimeMillis();
            task1(session, resPath + "task1.txt");
            long endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_______________TASK2______________");
            startTime = System.currentTimeMillis();
            task2(session, resPath + "task2.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_______________TASK3______________");
            startTime = System.currentTimeMillis();
            task3(session, resPath + "task3.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_______________TASK4______________");
            startTime = System.currentTimeMillis();
            task4(session, "Kyiv", resPath + "task4.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_______________TASK5______________");
            startTime = System.currentTimeMillis();
            task5(session, resPath + "task5.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_______________TASK5.2______________");
            startTime = System.currentTimeMillis();
            task5_2(session, resPath + "task5-1.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
