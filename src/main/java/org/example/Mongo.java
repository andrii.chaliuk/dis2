package org.example;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.mongodb.MongoException;
import com.mongodb.client.*;
import com.mongodb.client.model.Aggregates;
import com.mongodb.client.model.Filters;
import org.bson.Document;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class Mongo {

    private static final String resPath = "src/main/results/mongo/";

    private static final String dataFile = "src/main/docker/CV.json";

    private static final Random random = new Random();

    public static void generateTestData() throws IOException {
        int usersNum = 10000;
        int industriesNum = 10000;
        int jobNum = 10000;
        int companyNum = 10000;
        int jobRecordNum = 10000;
        int jobRecordResumeNum = 100000;
        int contactTypeNum = 10000;
        int contactInfoNum = 10000;
        int hobbyNum = 10000;
        int resumeNum = 10000;
        int hobbyResumeNum = 100000;

        String[] cities = {"Lviv", "Kyiv", "Odesa", "Kharkiv", "A", "B", "C", "D", "E", "F", "G"};

        FileWriter fileWriter = new FileWriter(dataFile);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        printWriter.println("[");
        for (int i = 0; i < resumeNum; ++i) {
            int user = random.nextInt(usersNum);
            int position = random.nextInt(jobNum);
            int industry = random.nextInt(industriesNum);
            int city = random.nextInt(cities.length);
            int jobRecordsNum = random.nextInt(100);
            StringBuilder jobRecords = new StringBuilder();
            for (int j = 0; j < jobRecordsNum; ++j) {
                int record = random.nextInt(jobRecordNum);
                int company = random.nextInt(companyNum);
                jobRecords.append("{\n\t\t\t\"id\":").append(record).append(", ").append("\n\t\t\t\"company\": ").append("{\n\t\t\t\t\"id\": ").append(company).append(", \n\t\t\t\t\"company_name\": \"Company №").append(company).append("\"\n\t\t\t},\n\t\t\t\"start_date\": \"2020-09-01\",\n").append("\t\t\t\"end_date\": \"2024-06-30\"\n\t\t}");
                if (j < jobRecordsNum - 1) {
                    jobRecords.append(",\n\t\t");
                }
            }

            int company = random.nextInt(companyNum);
            int contactInfo = random.nextInt(contactInfoNum);
            int contactType = random.nextInt(contactTypeNum);

            int hobbiesNum = random.nextInt(100);
            StringBuilder hobbies = new StringBuilder();
            for (int j = 0; j < hobbiesNum; ++j) {
                int hobby = random.nextInt(hobbyNum);
                hobbies.append("{\n\t\t\t\"id\": ").append(hobby).append(", \n\t\t\t\"hobby_type\": \"Hobby").append(hobby).append("\"\n\t\t}");
                if (j < hobbiesNum - 1) {
                    hobbies.append(",\n\t\t");
                }
            }
            printWriter.printf("""
                            {
                                "id": %d,
                                "first_name": "%s",
                                "last_name": "%s",
                                "user" : {
                                    "id": %d,
                                    "login": "%s",
                                    "password": "%s"
                                },
                                "position": {
                                    "id": %d,
                                    "job_title": "%s"
                                },
                                "industry": {
                                    "id": %d,
                                    "industry_name": "%s"
                                },
                                "city": {
                                    "id": %d,
                                    "city_name": "%s"
                                },
                                "date_of_creation": "2023-10-01",
                                "job_records": [
                                    %s
                                ],
                                "company": {
                                    "id": %d,
                                    "company_name": "%s"
                                },
                                "contact_info": {
                                    "id": %d,
                                    "type": {
                                        "id": %d,
                                        "type": "%s"
                                    },
                                    "url": "%s"
                                },
                                "hobbies": [
                                    %s
                                ]
                            }
                            """, i, "FirstName" + i, "LastName" + i,
                    user, "Login" + user, "Password" + user,
                    position, "Position" + position,
                    industry, "Industry" + industry,
                    city, cities[city], jobRecords.toString(), company, "Company #" + company,
                    contactInfo, contactType, "Type" + contactType, "URL" + contactInfo, hobbies.toString()
            );
            if (i < resumeNum - 1) {
                printWriter.println(",");
            }
        }
        printWriter.println("]");
        printWriter.close();
    }

    public static void task1(MongoCollection<Document> collection, String fileName) throws IOException {
        FindIterable<Document> result = collection.find();

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        for (Document doc : result) {
            String res = doc.toJson();
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task2(MongoCollection<Document> collection, String fileName) throws IOException {
        Document query = new Document();
        query.put("hobbies", new Document("$exists", true));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        for (Document document : collection.find(query)) {
            Object hobbies = document.get("hobbies");
            String res = "Hobbies: " + hobbies;
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task2_1(MongoCollection<Document> collection, String fileName) throws IOException {
        AggregateIterable<Document> result = collection.aggregate(Arrays.asList(
                Aggregates.unwind("$hobbies"),
                Aggregates.group("$hobbies.hobby_type"),
                Aggregates.project(new Document("_id", 0).append("hobby_type", "$_id"))
        ));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        for (Document document : result) {
            String hobby = document.getString("hobby_type");
            String res = "Hobby: " + hobby;
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task3(MongoCollection<Document> collection, String fileName) throws IOException {
        AggregateIterable<Document> result = collection.aggregate(Arrays.asList(
                Aggregates.unwind("$city"),
                Aggregates.group("$city.city_name"),
                Aggregates.project(new Document("_id", 0).append("city_name", "$_id"))
        ));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        for (Document document : result) {
            String city = document.getString("city_name");
            String res = "City: " + city;
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task4(MongoCollection<Document> collection, String targetCity, String fileName) throws IOException {
        AggregateIterable<Document> result = collection.aggregate(Arrays.asList(
                Aggregates.match(Filters.eq("city.city_name", targetCity)),
                Aggregates.unwind("$hobbies"),
                Aggregates.group("$hobbies.hobby_type"),
                Aggregates.project(new Document("_id", 0).append("hobby_type", "$_id"))
        ));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        for (Document document : result) {
            String hobby = document.getString("hobby_type");
            String res = "Hobby in city " + targetCity + ": " + hobby;
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task5(MongoCollection<Document> collection, String fileName) throws IOException {
        AggregateIterable<Document> result = collection.aggregate(
                Arrays.asList(
                        new Document("$unwind", "$job_records"),
                        new Document("$group", new Document("_id", "$job_records.company.company_name").append("applicants", new Document("$push", "$$ROOT"))),
                        new Document("$match", new Document("applicants.1", new Document("$exists", true)))
                )
        ).allowDiskUse(true);

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        for (Document document : result) {
            if (fileName.isEmpty()) {
                System.out.println(document.get("_id"));
            } else {
                printWriter.println(document.get("_id"));
            }
            for (Document applicant : (List<Document>) document.get("applicants")) {
                String res = applicant.get("first_name") + " " + applicant.get("last_name");
                if (fileName.isEmpty()) {
                    System.out.println(res);
                } else {
                    printWriter.println(res);
                }
            }
            if (fileName.isEmpty()) {
                System.out.println("--------");
            } else {
                printWriter.println("--------");
            }
        }
    }

    public static void main(String[] args) throws IOException {

        // generateTestData();

        String connectionString = "mongodb://localhost:27017/";

        try (MongoClient mongoClient = MongoClients.create(connectionString)) {
            MongoDatabase database = mongoClient.getDatabase("DISMongo");

            MongoCollection<Document> collection = database.getCollection("CV");

//            ObjectMapper objectMapper = new ObjectMapper();
//            File jsonFile = new File("src/main/docker/CV.json");
//
//            try {
//                Document[] documents = objectMapper.readValue(jsonFile, Document[].class);
//
//                for (Document document : documents) {
//                    collection.insertOne(document);
//                }
//
//                System.out.println("JSON data from file added to MongoDB collection.");
//
//            } catch (IOException e) {
//                System.err.println("Error reading JSON file: " + e.getMessage());
//            }
            System.out.println("________________TASK1_______________");
            long startTime = System.currentTimeMillis();
            task1(collection, resPath + "task1.txt");
            long endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("________________TASK2_______________");
            startTime = System.currentTimeMillis();
            task2(collection, resPath + "task2.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("________________TASK2.1_______________");
            startTime = System.currentTimeMillis();
            task2_1(collection, resPath + "task2-1.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("________________TASK3_______________");
            startTime = System.currentTimeMillis();
            task3(collection, resPath + "task3.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("________________TASK4_______________");
            startTime = System.currentTimeMillis();
            task4(collection, "Kyiv", resPath + "task4.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("________________TASK5_______________");
            startTime = System.currentTimeMillis();
            task5(collection, resPath + "task5.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            mongoClient.close();

        } catch (MongoException e) {
            System.err.println("MongoDB connection error: " + e.getMessage());
        }
    }
}
