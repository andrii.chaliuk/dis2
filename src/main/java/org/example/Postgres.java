package org.example;

import java.io.*;
import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;
import java.util.Scanner;

public class Postgres {

    private static final String resPath = "src/main/results/postgres/";
    private static final String dataFile = "src/main/docker/postgres-init.sql";
    private static final String schemaFile = "src/main/docker/postgres-schema.sql";
    private static final Random random = new Random();

    public static void generateTestData() throws IOException {
        int usersNum = 10000;
        int industriesNum = 10000;
        int jobNum = 10000;
        int companyNum = 10000;
        int jobRecordNum = 10000;
        int jobRecordResumeNum = 100000;
        int contactTypeNum = 10000;
        int contactInfoNum = 10000;
        int hobbyNum = 10000;
        int resumeNum = 10000;
        int hobbyResumeNum = 100000;

        String[] cities = {"Lviv", "Kyiv", "Odesa", "Kharkiv", "A", "B", "C", "D", "E", "F", "G"};

        FileWriter fileWriter = new FileWriter(dataFile);
        PrintWriter printWriter = new PrintWriter(fileWriter);

        try {
            File myObj = new File(schemaFile);
            Scanner myReader = new Scanner(myObj);
            while (myReader.hasNextLine()) {
                String data = myReader.nextLine();
                printWriter.println(data);
            }
            myReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

        for (int i = 0; i < usersNum; ++i) {
            printWriter.print("INSERT INTO \"users\" (\"id\", \"login\", \"password\") VALUES (" + i + ", 'user" + i + "', 'password + " + i + "');\n");
        }

        for (int i = 0; i < cities.length; ++i) {
            printWriter.printf("INSERT INTO \"cities\" (\"id\", \"city_name\") VALUES (" + i + ", '" + cities[i] + "');\n");
        }

        for (int i = 0; i < industriesNum; ++i) {
            printWriter.printf("INSERT INTO \"industries\" (\"id\", \"industry_name\") VALUES (" + i + ", 'Industry" + i + "');\n");
        }

        for (int i = 0; i < jobNum; ++i) {
            printWriter.printf("INSERT INTO \"jobs\" (\"id\", \"job_title\") VALUES (" + i + ", 'Job" + i + "');\n");
        }

        for (int i = 0; i < companyNum; ++i) {
            printWriter.printf("INSERT INTO \"company\" (\"id\", \"company_name\") VALUES (" + i + ", 'Company №" + i + "');\n");
        }

        for (int i = 0; i < jobRecordNum; ++i) {
            printWriter.printf("INSERT INTO \"job_record\" (\"id\", \"company_id\", \"start_date\", \"end_date\") VALUES (" + i + ", " + random.nextInt(companyNum) + ", '2020-09-01', '2024-06-30');\n");
        }

        for (int i = 0; i < contactTypeNum; ++i) {
            printWriter.printf("INSERT INTO \"contact_types\" (\"id\", \"type\") VALUES (" + i + ", 'ContactType" + i + "');\n");
        }

        for (int i = 0; i < contactInfoNum; ++i) {
            printWriter.printf("INSERT INTO \"contact_infos\" (\"id\", \"type_id\", \"url\") VALUES (" + i + ", " + random.nextInt(contactTypeNum) + ", 'user1@example.com');\n");
        }

        for (int i = 0; i < hobbyNum; ++i) {
            printWriter.printf("INSERT INTO \"hobbies\" (\"id\", \"hobby_type\") VALUES (" + i + ", 'Hobby" + i + "');\n");
        }

        for (int i = 0; i < resumeNum; ++i) {
            printWriter.printf("INSERT INTO \"resume\" (\"id\", \"first_name\", \"last_name\", \"user_id\"," +
                    " \"position_id\", \"inductry_id\", \"city_id\", \"company_id\", \"contact_info_id\", \"date_of_creation\") VALUES" +
                    " (" + i + ", 'FirstName" + i + "', 'LastName" + i + "', " + random.nextInt(usersNum) + ", "
                    + random.nextInt(jobNum) + ", " + random.nextInt(industriesNum) +
                    ", " + random.nextInt(cities.length) + ", " + random.nextInt(companyNum) + ", " +
                    random.nextInt(contactInfoNum) + ", '2023-10-01');\n");
        }

        HashMap<Integer, ArrayList<Integer>> map = new HashMap<Integer, ArrayList<Integer>>();
        for (int i = 0; i < jobRecordResumeNum; ++i) {
            int jobRecordId = random.nextInt(jobRecordNum);
            int resumeId = random.nextInt(resumeNum);
            ArrayList<Integer> arr = map.get(jobRecordId);
            if (arr == null || !arr.contains(resumeId)) {
                printWriter.printf("INSERT INTO \"job_record_resume\" (\"job_record_id\", \"resume_id\") VALUES ("
                        + jobRecordId + ", " + resumeId + ");\n");
                if (arr == null) {
                    arr = new ArrayList<Integer>();
                }
                arr.add(resumeId);
                map.put(jobRecordId, arr);
            }
        }

        map = new HashMap<Integer, ArrayList<Integer>>();
        for (int i = 0; i < hobbyResumeNum; ++i) {
            int hobbyId = random.nextInt(hobbyNum);
            int resumeId = random.nextInt(resumeNum);
            ArrayList<Integer> arr = map.get(hobbyId);
            if (arr == null || !arr.contains(resumeId)) {
                printWriter.printf("INSERT INTO \"hobby_resume\" (\"hobby_id\", \"resume_id\") VALUES (" +
                        hobbyId + ", " + resumeId + ");\n");
                if (arr == null) {
                    arr = new ArrayList<>();
                }
                arr.add(resumeId);
                map.put(hobbyId, arr);
            }
        }

        printWriter.close();
    }

    public static void task1(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT\n" +
                        "    r.id AS Resume_ID,\n" +
                        "    r.first_name AS First_Name,\n" +
                        "    r.last_name AS Last_Name,\n" +
                        "    u.login AS Login,\n" +
                        "    c.city_name AS City,\n" +
                        "    i.industry_name AS Industry,\n" +
                        "    j.job_title AS Job_Title,\n" +
                        "    e.company_id AS Company_Id,\n" +
                        "    e.start_date AS Job_Start_Date,\n" +
                        "    e.end_date AS Job_End_Date,\n" +
                        "    h.hobby_type AS Hobby_Type\n" +
                        "FROM resume r\n" +
                        "LEFT JOIN users u ON r.user_id = u.id\n" +
                        "LEFT JOIN cities c ON r.city_id = c.id\n" +
                        "LEFT JOIN industries i ON r.inductry_id = i.id\n" +
                        "LEFT JOIN jobs j ON r.position_id = j.id\n" +
                        "LEFT JOIN job_record_resume er ON r.id = er.resume_id\n" +
                        "LEFT JOIN job_record e ON er.job_record_id = e.id\n" +
                        "LEFT JOIN hobby_resume hr ON r.id = hr.resume_id\n" +
                        "LEFT JOIN hobbies h ON hr.hobby_id = h.id;\n";
        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            int id = resultSet.getInt("Resume_ID");
            String firstName = resultSet.getString("First_Name");
            String lastName = resultSet.getString("Last_Name");
            String userName = resultSet.getString("Login");
            String cityName = resultSet.getString("City");
            String industryName = resultSet.getString("Industry");
            String jobTitle = resultSet.getString("Job_Title");
            String hobby = resultSet.getString("Hobby_Type");

            String companyId = resultSet.getString("Company_Id");
            String res = "{\n\tID: " + id + ",\n\tfirst name: " + firstName + ",\n\tlast name: " + lastName +
                    ",\n\tusername: " + userName + ",\n\tcity: " + cityName + ",\n\tcompany id: " + companyId +
                    ",\n\tindustry: " + industryName + ",\n\tjob: " + jobTitle + ",\n\thobby: " + hobby + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }

        resultSet.close();
        preparedStatement.close();
    }

    public static void task1_2(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery = "SELECT * " +
                "FROM resume r;\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            int id = resultSet.getInt("id");
            String firstName = resultSet.getString("first_name");
            String lastName = resultSet.getString("last_name");
            String res = "{\n\tID: " + id + ",\n\tfirst name: " + firstName + ",\n\tlast name: " + lastName + "\t}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task2(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT DISTINCT h.hobby_type AS Hobby\n" +
                        "FROM hobbies h\n" +
                        "INNER JOIN hobby_resume hr ON h.id = hr.hobby_id;\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String hobby = resultSet.getString("Hobby");
            String res = "{\n\tHobby: " + hobby + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task3(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT DISTINCT c.id, c.city_name \n" +
                        "FROM resume r\n" +
                        "INNER JOIN cities c ON c.id = r.city_id;\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String city = resultSet.getString("city_name");
            String res = "{\n\tCity: " + city + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task4(Connection connection, String city, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT DISTINCT h.hobby_type AS Hobby\n" +
                        "FROM hobbies h\n" +
                        "INNER JOIN hobby_resume hr ON h.id = hr.hobby_id\n" +
                        "INNER JOIN resume r ON hr.resume_id = r.id\n" +
                        "INNER JOIN cities c ON r.city_id = c.id\n" +
                        "WHERE c.city_name = ?;\n";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);
        preparedStatement.setString(1, city);

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        while (resultSet.next()) {
            String hobby = resultSet.getString("Hobby");
            String res = "{\n\tHobby: " + hobby + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void task5(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT DISTINCT u.*, r.last_name, r.first_name, e.company_id\n" +
                        "FROM users u\n" +
                        "INNER JOIN resume r ON u.id = r.user_id\n" +
                        "INNER JOIN job_record_resume er ON r.id = er.resume_id\n" +
                        "INNER JOIN job_record e ON er.job_record_id = e.id\n" +
                        "WHERE EXISTS (\n" +
                        "    SELECT 1\n" +
                        "    FROM job_record_resume er2\n" +
                        "    INNER JOIN job_record e2 ON er2.job_record_id = e2.id\n" +
                        "    WHERE e2.company_id = e.company_id\n" +
                        "    AND er2.resume_id <> er.resume_id\n" +
                        ");";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String login = resultSet.getString("login");
            String lastName = resultSet.getString("last_name");
            String firstName = resultSet.getString("first_name");
            String company = resultSet.getString("company_id");
            String res = "{\n\tlogin: " + login + ",\n\tlastName: " + lastName + ",\n\tfirstname: " + firstName +
                    ",\n\tcompany_id: " + company + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }


    public static void task5_1(Connection connection, String fileName) throws SQLException, IOException {
        String selectQuery =
                "SELECT DISTINCT u.*, r.last_name, r.first_name, e.company_id\n" +
                        "FROM users u\n" +
                        "INNER JOIN resume r ON u.id = r.user_id\n" +
                        "INNER JOIN job_record_resume er ON r.id = er.resume_id\n" +
                        "INNER JOIN job_record e ON er.job_record_id = e.id\n" +
                        "ORDER BY e.company_id ASC";

        PreparedStatement preparedStatement = connection.prepareStatement(selectQuery);

        long startTime = System.currentTimeMillis();
        ResultSet resultSet = preparedStatement.executeQuery();
        long endTime = System.currentTimeMillis();
        System.out.println("Query: " + (endTime - startTime));

        FileWriter fileWriter = null;
        PrintWriter printWriter = null;
        if (!fileName.isEmpty()) {
            File file = new File(fileName);
            file.createNewFile();
            fileWriter = new FileWriter(file);
            printWriter = new PrintWriter(fileWriter);
        }

        while (resultSet.next()) {
            String login = resultSet.getString("login");
            String lastName = resultSet.getString("last_name");
            String firstName = resultSet.getString("first_name");
            String company = resultSet.getString("company_id");
            String res = "{\n\tlogin: " + login + ",\n\tlastName: " + lastName + ",\n\tfirstname: " + firstName +
                    ",\n\tcompany: " + company + "\n}";
            if (fileName.isEmpty()) {
                System.out.println(res);
            } else {
                printWriter.println(res);
            }
        }

        resultSet.close();
        preparedStatement.close();

        if (!fileName.isEmpty()) {
            fileWriter.close();
        }
    }

    public static void main(String[] args) throws IOException {

        // generateTestData();

        try {
            String url = "jdbc:postgresql://localhost:15432/postgres";
            String user = "postgres";
            String password = "postgres";
            Class.forName("org.postgresql.Driver");

            Connection connection = DriverManager.getConnection(url, user, password);

            System.out.println("_________________Task1___________________");
            long startTime = System.currentTimeMillis();
            task1(connection, resPath + "task1.txt");
            long endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            startTime = System.currentTimeMillis();
            task1_2(connection, resPath + "task1-2.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task2___________________");
            startTime = System.currentTimeMillis();
            task2(connection, resPath + "task2.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task3___________________");
            startTime = System.currentTimeMillis();
            task3(connection, resPath + "task3.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task4___________________");
            startTime = System.currentTimeMillis();
            task4(connection, "Kharkiv", resPath + "task4.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task5___________________");
            startTime = System.currentTimeMillis();
            task5(connection, resPath + "task5.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            System.out.println("_________________Task5.1___________________");
            startTime = System.currentTimeMillis();
            task5_1(connection, resPath + "task5-1.txt");
            endTime = System.currentTimeMillis();
            System.out.println("time: " + (endTime - startTime));

            connection.close();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.err.println("PostgreSQL JDBC driver not found");
        } catch (SQLException e) {
            e.printStackTrace();
            System.err.println("Database connection error");
        }
    }
}
