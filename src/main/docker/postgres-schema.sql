CREATE TABLE "users" (
                         "id" integer PRIMARY KEY,
                         "login" varchar UNIQUE,
                         "password" varchar
);

CREATE TABLE "cities" (
                          "id" integer PRIMARY KEY,
                          "city_name" varchar
);

CREATE TABLE "industries" (
                              "id" integer PRIMARY KEY,
                              "industry_name" varchar
);

CREATE TABLE "jobs" (
                        "id" integer PRIMARY KEY,
                        "job_title" varchar
);

CREATE TABLE "company" (
                           "id" integer PRIMARY KEY,
                           "company_name" varchar
);

CREATE TABLE "job_record" (
                              "id" integer PRIMARY KEY,
                              "company_id" integer,
                              "start_date" timestamp,
                              "end_date" timestamp
);

CREATE TABLE "job_record_resume" (
                                     "job_record_id" integer,
                                     "resume_id" integer,
                                     PRIMARY KEY ("job_record_id", "resume_id")
);

CREATE TABLE "contact_types" (
                                 "id" integer PRIMARY KEY,
                                 "type" varchar NOT NULL
);

CREATE TABLE "contact_infos" (
                                 "id" integer PRIMARY KEY,
                                 "type_id" integer,
                                 "url" varchar(255) NOT NULL
);

CREATE TABLE "hobbies" (
                           "id" integer PRIMARY KEY,
                           "hobby_type" varchar(255) NOT NULL
);

CREATE TABLE "hobby_resume" (
                                "hobby_id" integer,
                                "resume_id" integer,
                                PRIMARY KEY ("hobby_id", "resume_id")
);

CREATE TABLE "resume" (
                          "id" integer PRIMARY KEY,
                          "first_name" varchar(255) NOT NULL,
                          "last_name" varchar(255) NOT NULL,
                          "user_id" integer,
                          "position_id" integer,
                          "inductry_id" integer,
                          "company_id" integer,
                          "city_id" integer,
                          "contact_info_id" integer,
                          "date_of_creation" timestamp
);

ALTER TABLE "resume" ADD FOREIGN KEY ("user_id") REFERENCES "users" ("id");

ALTER TABLE "resume" ADD FOREIGN KEY ("position_id") REFERENCES "jobs" ("id");

ALTER TABLE "resume" ADD FOREIGN KEY ("inductry_id") REFERENCES "industries" ("id");

ALTER TABLE "resume" ADD FOREIGN KEY ("city_id") REFERENCES "cities" ("id");

ALTER TABLE "resume" ADD FOREIGN KEY ("company_id") REFERENCES "company" ("id");

ALTER TABLE "resume" ADD FOREIGN KEY ("contact_info_id") REFERENCES "contact_infos" ("id");

ALTER TABLE "hobby_resume" ADD FOREIGN KEY ("hobby_id") REFERENCES "hobbies" ("id");

ALTER TABLE "hobby_resume" ADD FOREIGN KEY ("resume_id") REFERENCES "resume" ("id");

ALTER TABLE "contact_infos" ADD FOREIGN KEY ("type_id") REFERENCES "contact_types" ("id");

ALTER TABLE "job_record_resume" ADD FOREIGN KEY ("job_record_id") REFERENCES "job_record" ("id");

ALTER TABLE "job_record_resume" ADD FOREIGN KEY ("resume_id") REFERENCES "resume" ("id");

ALTER TABLE "job_record" ADD FOREIGN KEY ("company_id") REFERENCES "company" ("id");